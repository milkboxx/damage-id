//
//  LicencePlateViewController.h
//  DamageID
//
//  Created by Davor Galambos on 19/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LoginViewController;
@interface LicencePlateViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, assign) IBOutlet UIButton *btnStart;
@property (nonatomic, assign) IBOutlet UIButton *btnLogout;
@property (nonatomic, assign) IBOutlet UIScrollView *scrollView;
@property (nonatomic, assign) IBOutlet UIImageView *imgBackground;
@property (nonatomic, assign) IBOutlet UITextField *txtfUnitNumber;
@property (nonatomic, assign) IBOutlet UITextField *txtfLicensePlate;
@property (nonatomic, assign) IBOutlet UILabel *lblVersion;

@property (nonatomic, assign) IBOutlet UILabel *retryLabel;
@property (nonatomic, assign) IBOutlet UIButton *retryButton;
@property (nonatomic, retain) LoginViewController *loginVC;
@end
