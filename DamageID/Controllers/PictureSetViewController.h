//
//  PictureSetViewController.h
//  DamageID
//
//  Created by Davor Galambos on 25/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CammeraOverlay.h"
@class CaseModel, CammeraOverlay;
@interface PictureSetViewController : UIViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, DamageCammeraProtocol>

@property (nonatomic, weak) IBOutlet UIImageView *imgThumbnail;
@property (nonatomic, strong) CaseModel *editor;
@property (nonatomic, assign) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImagePickerController *imageController;
@property (nonatomic, strong) CammeraOverlay *cammeraOverlay;
@property (nonatomic, weak) IBOutlet UIButton *btnDone;
@property (nonatomic, strong) IBOutlet UIView *viewCDW;
@property (nonatomic, strong) IBOutlet UIView *viewBtnCDW;


@end
