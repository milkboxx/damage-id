//
//  LoginViewController.m
//  DamageID
//
//  Created by Davor Galambos on 08/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "LoginViewController.h"
#import "ApiClient.h"
#import "LoginModel.h"
#import "LicencePlateViewController.h"
#import "AppDelegate.h"


@interface LoginViewController () {
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtfPassword.text = @"";
    _txtfUsername.text = @"";

    //_txtfPassword.text = @"bat27r3ntaL";
    //_txtfUsername.text = @"brucewayne";
    
    //_txtfUsername.text = @"StatusGroupAdmin1";
    //_txtfPassword.text = @"statusgroup11";
    [self setEdgesForExtendedLayout:UIRectEdgeNone];

    

    UIImage *imgButton=[[UIImage imageNamed:@"start_button"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];
    [_btnSignIn setBackgroundImage:imgButton forState:UIControlStateNormal];
    
    [self observeKeyboardNotifications];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;

    _scrollView.contentSize = self.view.bounds.size;
    _imgBackground.frame=CGRectMake(0, -64, self.view.bounds.size.width, self.view.bounds.size.height+64);
}



- (void)dealloc {
    //stop observing
    [self stopObervingKeyboard];
}

#pragma Keyboard methods
- (void)observeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)stopObervingKeyboard {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)keyboardWillShow:(NSNotification *)notification {
    float kbHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    CGRect frame=_scrollView.frame;
    frame.size.height=self.view.bounds.size.height-kbHeight;
    _scrollView.frame=frame;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    CGRect frame=_scrollView.frame;
    frame.size.height=self.view.bounds.size.height;
    _scrollView.frame=frame;
    
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark Actions

- (IBAction)btnSignInPressed:(id)sender{
    [_txtfUsername resignFirstResponder];
    [_txtfPassword resignFirstResponder];
    [self signIn];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if(textField == _txtfUsername){
        [_txtfPassword becomeFirstResponder];
    } else {
        [self signIn];
    }
    return YES;
}

- (void)signIn{
    [self btnSignInSetSigningIn:YES];
    [[ApiClient sharedInstance] logInForUsername:_txtfUsername.text andPassword:_txtfPassword.text onSuccess:^(LoginModel *result) {
        [self btnSignInSetSigningIn:NO];
        [[ApiClient sharedInstance] updateAccessKey:result.accessKey];
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        delegate.photoVersion = result.photoVersion;
        LicencePlateViewController *vc=[[LicencePlateViewController alloc] initWithNibName:XIB(@"LicencePlateViewController") bundle:nil];
        vc.loginVC = self;
        [self.navigationController pushViewController:vc animated:YES];
    } onFailure:^(NSError *error) {
        if(error.code == -1011){
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Please enter correct Username or Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self btnSignInSetSigningIn:NO];
    }];
}

-(void)btnSignInSetSigningIn:(BOOL)isSigning{
    [_btnSignIn setTitle: isSigning ? @"SIGNING IN..." : @"SIGN IN" forState:UIControlStateNormal];
    [_btnSignIn setEnabled:!isSigning];
}

-(IBAction)btnLostPasswordSelect:(id)sender{
    
    NSURL *url = [NSURL URLWithString:@"https://app.damageid.com/password/forgot"];
    
    if (![[UIApplication sharedApplication] openURL:url]) {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }
}


- (NSUInteger) application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL) shouldAutorotate {
    return NO;
}

@end
