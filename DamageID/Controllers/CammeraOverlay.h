//
//  CammeraOverlay.h
//  DamageID
//
//  Created by Davor Galambos on 27/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DamageCammeraProtocol
- (void)damageChanged:(UISwitch *)sender forTag:(long)tag;
- (void)tempDamageChanged:(UISwitch *)sender forTag:(long)tag;
@end

@interface CammeraOverlay : UIView


@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) IBOutlet UISwitch *swDamage;
@property (nonatomic, strong) IBOutlet UIImageView *imgOverlay;
@property (nonatomic, strong) IBOutlet UILabel *lblDamage;
@property (nonatomic, strong) NSString *overlayImgName;
@property (nonatomic, assign) long imageTag;
@property (nonatomic, assign) id<DamageCammeraProtocol> delegate;
@property (nonatomic, assign) BOOL initialDamage;


- (void)hideDamageSwitch;
- (void)afterInit;

@end
