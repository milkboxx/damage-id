//
//  FullScreenViewController.h
//  DamageID
//
//  Created by Davor Galambos on 28/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CammeraOverlay.h"

@interface FullScreenViewController : UIViewController

//@property (nonatomic, strong) NSString *imgPath;
@property (nonatomic, strong) NSData *imgData;

@property (nonatomic, strong) IBOutlet UIImageView *imgFullscreen;
@property (nonatomic, assign) long imageTag;
@property (nonatomic, assign) id<DamageCammeraProtocol> delegate;
@property (nonatomic, strong) IBOutlet UISwitch *swDamage;
//@property (nonatomic, assign) IBOutlet UIButton *btnClose;
@property (nonatomic, assign) BOOL isDamageSelected;

-(void)hideDamageSwitch;


@end
