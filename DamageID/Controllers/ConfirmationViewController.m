//
//  ConfirmationViewController.m
//  DamageID
//
//  Created by Davor Galambos on 23/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "ConfirmationViewController.h"
#import "CaseModel.h"
#import "UIAlertView+BlocksKit.h"
#import "PictureSetViewController.h"
#import "DBHelper.h"

@interface ConfirmationViewController (){
    BOOL selectBefore;
}
@end

@implementation ConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self btnReturnSelected:nil];
    [self.navigationItem setTitle:@"Check In"];
    _lblModel.text = _editor.model;
    _lblUnitNumber.text = [NSString stringWithFormat:@"#%@", _editor.unitNumber];
   /* [_btnRadioNewCase setImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [_btnRadioReturn setImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [_btnRadioReturn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 100)];*/
    
    UIImage *imgButton=[[UIImage imageNamed:@"checkin_no_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];
    [_btnContinue setBackgroundImage:imgButton forState:UIControlStateNormal];

    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UINavigationController *nc=self.navigationController;
    [nc.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    nc.navigationBar.shadowImage = [UIImage new];
    nc.navigationBar.translucent = YES;
    nc.view.backgroundColor = [UIColor clearColor];
    nc.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnReturnSelected:(id)sender{
    selectBefore = NO;
    [_btnRadioReturn setImage:[UIImage imageNamed:@"radio_on"] forState:UIControlStateNormal];
    [_btnRadioNewCase setImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    
}

- (IBAction)btnNewCaseSelected:(id)sender{
    selectBefore = YES;
    [_btnRadioReturn setImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [_btnRadioNewCase setImage:[UIImage imageNamed:@"radio_on"] forState:UIControlStateNormal];
}

- (IBAction)btnContinueSelected:(id)sender{
    if(selectBefore){
        [self takeBefore];
    } else {
        [self takeAfter];
        
    }
}

- (void)takeBefore{
    //BOOL takenBeforeButNotYetUploaded = NO;
    BOOL takenBeforeButNotYetUploaded = [[DBHelper sharedInstance] checkIfCaseTaken:_editor.nextCaseNumber andBeforeAfter:0];
    /**
     * If there already are before photos then createAreYouSureBeforeDialog which asks you
     * if you want to create new case and take before photos
     */
    if ((_editor.isNewCase == NO && _editor.photosBefore > 0) || takenBeforeButNotYetUploaded == YES){
        [self createAreYouSureAfterDialog];
    }
    else {
        _editor.beforeAfter = 0;
        [self openPictureSet];

    }
}

-(void)createAreYouSureAfterDialog{
    UIAlertView *alertView = [UIAlertView alertWithTitle:@"Alert" message: @"You already have \"Before\" pictures, do you want to create new case?"];
    [alertView addButtonWithTitle:@"Yes" handler:^{
        [self checkinCreateCase];
    }];
    [alertView addButtonWithTitle:@"No" handler:^{  }];
    [alertView show];
}


-(void)takeAfter{
    if (_editor.isNewCase == NO && _editor.photosAfter > 0){
        //unreachable code
        [self createAreYouSureAfterDialog];
    }
    else {
        _editor.beforeAfter = 1;
        [self openPictureSet];
    }
}


-(void)checkinCreateCase{
    _editor.isNewCase = 1;
    _editor.beforeAfter = 0;
    _editor.photosBefore = 0;
    _editor.photosAfter = 0;
    
    [self openPictureSet];
}

-(void)openPictureSet{
    PictureSetViewController *vc=[[PictureSetViewController alloc] initWithNibName:@"PictureSetViewController" bundle:nil];
    vc.editor = self.editor;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
