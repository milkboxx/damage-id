//
//  FullScreenViewController.m
//  DamageID
//
//  Created by Davor Galambos on 28/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "FullScreenViewController.h"
#import "AppDelegate.h"
@interface FullScreenViewController ()

@end

@implementation FullScreenViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_swDamage setOnTintColor:AccentRedColor];
    [_swDamage setOn:_isDamageSelected];
    [_swDamage addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    //[_btnClose setTintColor:AccentBlueColor];
    //if(_imgPath){
    //    NSData *data=[NSData dataWithContentsOfFile:_imgPath];
        if(_imgData)
            _imgFullscreen.image = [UIImage imageWithData:_imgData];
    //}

    self.view.backgroundColor = [UIColor blackColor];
    
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(closeModal:)];
    [close setTintColor:AccentRedColor];
    self.navigationItem.leftBarButtonItem = close;
}

- (void)viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)switchChanged:(id)sender{
    [_delegate damageChanged:sender forTag:_imageTag];
}


- (IBAction)closeModal:(id)sender{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.fullScreenVideo = NO;
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

-(void)hideDamageSwitch{
    [_swDamage setHidden:YES];
}

@end
