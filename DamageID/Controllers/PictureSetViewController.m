//
//  PictureSetViewController.m
//  DamageID
//
//  Created by Davor Galambos on 25/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "PictureSetViewController.h"
#import "CaseModel.h"
#import "CaseCreateModel.h"
#import "UIImageView+XLProgressIndicator.h"
#import "CammeraOverlay.h"
#import "UIActionSheet+BlocksKit.h"
#import "FullScreenViewController.h"
#import "AppDelegate.h"
#import "AFAmazonS3Manager.h"
#import "AFAmazonS3RequestSerializer.h"
#import "AFAmazonS3ResponseSerializer.h"
#import "UIAlertView+BlocksKit.h"
#import "ApiClient.h"
#import "JCNotificationCenter.h"
#import "LicencePlateViewController.h"
#import "CaseEntity.h"
#import "DBHelper.h"
#import "PhotoEntity.h"
#import "DateFormat.h"

@interface PictureSetViewController (){
    long currentPhotoTag;
    NSMutableArray *contentData;
    BOOL tempDamage;
    long currentXpos;
    long currentYpos;
    long thumbnailSizeWH;
    long photoVersion;
    long totalNumberOfPhotos;
    //NSManagedObjectContext *context;
    CaseEntity *dbCaseEntry;
    BOOL damageSelection;
    long photoIndexDamage;
}

@end

@implementation PictureSetViewController

-(void)popToLicence{
    [self deleteCase];
    NSArray *viewStack=[self.navigationController.viewControllers copy];
    for(UIViewController * viewInStack in viewStack){
        if ([viewInStack class] == [LicencePlateViewController class]){
            [self.navigationController popToViewController:viewInStack animated:YES];
            break;
        }
    }
}

- (void)closeController:(id)sender{
    if (sender!=nil) {
        UIAlertView *alert = [UIAlertView alertWithTitle:@"Alert!" message:@"Are you sure you want to cancel? No photos will be saved."];
        [alert addButtonWithTitle:@"Yes" handler:^{
            [self deleteCaseAndPop];
        }];
        [alert addButtonWithTitle:@"No" handler:^{
            //Do nothing
        }];
        [alert show];
    } else {
        [self deleteCaseAndPop];
    }
}

-(void)deleteCaseAndPop{
    [self deleteCase];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dbCaseEntry = [[DBHelper sharedInstance] insertCaseWithCaseNumber: _editor.caseNumber
                                         nextCaseNumber: _editor.nextCaseNumber
                                            beforeAfter: _editor.beforeAfter > 0 ? @1 : @0
                                           licensePlate: _editor.licensePlate
                                                  model: _editor.model
                                              insurance: @0
                                              accessKey: [[ApiClient sharedInstance] tokenKey]];
    
    
    totalNumberOfPhotos=0;
    _viewBtnCDW.layer.cornerRadius=4;
        _viewCDW.alpha=0;
    CGRect frame=_viewCDW.frame;
    frame.origin.y=-1000;
    _viewCDW.frame=frame;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //context = delegate.managedObjectContext;
    photoVersion = delegate.photoVersion;
    if(_editor.thumbnailUrl != nil)
        [_imgThumbnail setImageWithProgressIndicatorAndURL:[NSURL URLWithString:_editor.thumbnailUrl]];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = _editor.beforeAfter == 0 ? @"Going Out" : @"Return" ;
    
    UILabel *idLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 40)];
    idLabel.textColor = AccentRedColor;
    idLabel.font = [UIFont boldSystemFontOfSize:20];
    idLabel.text = [NSString stringWithFormat:@"%@", _editor.unitNumber];
    [idLabel setMinimumScaleFactor:0.5];
    [idLabel setAdjustsFontSizeToFitWidth:YES];
    
    
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithCustomView:idLabel];
    
    UIImage *imgButton=[[UIImage imageNamed:@"checkin_no_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];
    [_btnDone setBackgroundImage:imgButton forState:UIControlStateNormal];


    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(closeController:)];
    self.navigationItem.leftBarButtonItem = close;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    if(!contentData){
        
        NSString *path = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat:@"PictureSetConfig%ld",photoVersion] ofType:@"plist"];
        NSPropertyListFormat format;
        contentData = [NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:path]
                                                        mutabilityOption:NSPropertyListMutableContainers
                                                                  format:&format
                                                        errorDescription:NULL];

        thumbnailSizeWH = (self.view.bounds.size.width-16*3)/2;
        currentXpos = 16;
        currentYpos = _imgThumbnail.frame.size.height + _imgThumbnail.frame.origin.y + 10;
        int i;
        for(i=0; i <= photoVersion ; i++)
            [self addThumbnail];
    }
}


- (void)addThumbnail{
    totalNumberOfPhotos+=1;
    NSDictionary *data=[contentData objectAtIndex:totalNumberOfPhotos-1];
    long tag = [[data objectForKey:@"Tag"] longValue];
    
    CGRect frame=CGRectMake(currentXpos, currentYpos , thumbnailSizeWH, thumbnailSizeWH);
    
    if (currentXpos > 16){
        currentXpos = 16;
        currentYpos = currentYpos + thumbnailSizeWH + 40;
    } else {
        currentXpos = currentXpos*2 + thumbnailSizeWH;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    UIImageView *imgBackView=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"thumb_wrapper"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch]];
    frame.origin=CGPointMake(0, 0);
    imgBackView.frame=frame;
    //[view addSubview:imgBackView];
    
    UIButton *selectButton = [[UIButton alloc] initWithFrame:frame];
    [selectButton setBackgroundImage:[[UIImage imageNamed:@"thumb_wrapper"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [view addSubview:selectButton];
    [selectButton setTag:tag];
    [selectButton addTarget:self action:@selector(takePhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    //UIImage *imgButton=[[UIImage imageNamed:@"checkin_no_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];
    //[_btnContinue setBackgroundImage:imgButton forState:UIControlStateNormal];
    UIImageView *damageImage=[[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width-35-15, 15, 35, 35)];
    damageImage.image = nil;//[UIImage imageNamed:@"damage_yes"];
    damageImage.tag = 200+tag;
    [view addSubview:damageImage];
    
    UIImageView *icon=[[UIImageView alloc] initWithImage:[UIImage imageNamed:[data objectForKey:@"Icon"]]];
    icon.frame=CGRectMake(30, 30, frame.size.width-60, frame.size.height-60);
    [selectButton addSubview:icon];
    [icon setContentMode:UIViewContentModeScaleAspectFit];
    
    UIImageView *photoPlaceholder = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, frame.size.width-20, frame.size.height-20)];
    photoPlaceholder.tag = 100+tag;
    [photoPlaceholder setContentMode:UIViewContentModeScaleAspectFill];
    [photoPlaceholder setClipsToBounds:YES];
    [selectButton addSubview:photoPlaceholder];
    
    
    
    UIImageView *titleBack = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"title_thumb_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6) resizingMode:UIImageResizingModeStretch]];
    titleBack.frame = CGRectMake(0, frame.size.height+5, frame.size.width, 20);
    [view addSubview:titleBack];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:titleBack.frame];
    titleLabel.tag = 300+tag;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    titleLabel.text = [data objectForKey:@"Title"];
    [view addSubview:titleLabel];
    
    [_scrollView addSubview:view];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, view.frame.origin.y+view.frame.size.height+40+64)];
    frame=_btnDone.frame;
    frame.origin.y= _scrollView.contentSize.height-64;
    _btnDone.frame = frame;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)takePhoto:(UIButton *)sender{
    long imageIndex=sender.tag;
    
    BOOL isSet = [[[contentData objectAtIndex:imageIndex-1] objectForKey:@"ImageSet"] boolValue];
    if(isSet){
        UIActionSheet *photoActionSheet = [UIActionSheet sheetWithTitle:@"What do you want to do with picture?"];
        //Review image controller handler
        [photoActionSheet addButtonWithTitle:@"Review" handler:^{
            FullScreenViewController *photoGallery = [[FullScreenViewController alloc] init];
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            photoGallery.delegate = self;
            photoGallery.imageTag = imageIndex;
            NSString *damage=[[contentData objectAtIndex:imageIndex-1] valueForKey: @"Damage"];
            BOOL isDamageOn=NO;
            if ([damage isEqualToString:@"yes"])
                isDamageOn = YES;
            photoGallery.isDamageSelected = isDamageOn;
            
            if (_editor.beforeAfter == 0) //before doesnt have damage
                [_cammeraOverlay hideDamageSwitch];
            
            PhotoEntity *photo=[[DBHelper sharedInstance] getPhotoForCase: dbCaseEntry andcurrentPhotoId:imageIndex];
            
            photoGallery.imgData = photo.photo;
            
            delegate.fullScreenVideo = YES;
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:photoGallery];
            
            [self presentViewController:nc animated:YES completion:^{ }];
        }];
        
        // photo retake handler
        [photoActionSheet addButtonWithTitle:@"Retake" handler:^{
            [self retakePicture:sender];
        }];
        
        //    [photoActionSheet setDestructiveButtonWithTitle:@"Delete" handler:^{
        //        NSString *thumbPath=[[contentData objectAtIndex:tag] valueForKey: @"Thumbnail"];
        //        [self removeImage:thumbPath];
        //        [[contentData objectAtIndex:tag] removeObjectForKey: @"Thumbnail"];
        //        [self updatePhotoThumb:tag];
        //    }];
        
        [photoActionSheet setCancelButtonWithTitle:@"Cancel" handler:^{
            // do nothing
        }];
        [photoActionSheet showInView:self.view];
    } else {
        //set for first time

        [self retakePicture:sender];
    }
}


- (void)retakePicture:(UIButton *)sender{
    currentPhotoTag= sender.tag;
    if(!_imageController)
        _imageController = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [_imageController setSourceType:UIImagePickerControllerSourceTypeCamera];

        //[_imageController setShowsCameraControls:NO];
        _imageController.modalPresentationStyle = UIModalPresentationFullScreen;
        //
        [self addCameraOverlay];
        _imageController.delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCameraOverlay) name:@"_UIImagePickerControllerUserDidRejectItem" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeCameraOverlay) name:@"_UIImagePickerControllerUserDidCaptureItem" object:nil];

        
        [self presentViewController:_imageController animated:TRUE completion:^{}];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Camera not available on this device" delegate:nil cancelButtonTitle:(@"OK") otherButtonTitles:nil];
        [alertView show];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            [_imageController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            
            //[_imageController setShowsCameraControls:NO];
            _imageController.modalPresentationStyle = UIModalPresentationFullScreen;
            //
            //[self addCameraOverlay];
            _imageController.delegate = self;
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCameraOverlay) name:@"_UIImagePickerControllerUserDidRejectItem" object:nil];
            
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeCameraOverlay) name:@"_UIImagePickerControllerUserDidCaptureItem" object:nil];
            
            
            [self presentViewController:_imageController animated:TRUE completion:^{}];
            
        }
    }
}

- (void)addCameraOverlay{
    _cammeraOverlay =[[[NSBundle mainBundle] loadNibNamed:@"CammeraOverlay" owner:self options:nil] objectAtIndex:0];
    _cammeraOverlay.imagePicker = _imageController;
    _cammeraOverlay.overlayImgName = [[contentData objectAtIndex:(currentPhotoTag-1)] objectForKey:@"Mask"];
    _cammeraOverlay.frame = self.view.bounds;
    _cammeraOverlay.imageTag = currentPhotoTag;
    _cammeraOverlay.delegate = self;

    
    if (_editor.beforeAfter == 0)
        [_cammeraOverlay hideDamageSwitch];
    else if(currentPhotoTag>photoVersion){
        _cammeraOverlay.initialDamage = YES;
    }
        [_cammeraOverlay afterInit];
    
    [_imageController setCameraOverlayView:_cammeraOverlay];
}

- (void)removeCameraOverlay{
    [_cammeraOverlay removeFromSuperview];
}

- (void) removePhotoObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//- (void)removeImage:(NSString *)filePath
//{
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSError *error;
//    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
//    if (!success){
//        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
//    }
//}



// Photo take delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSDateFormatter *dateFormatter=[DateFormat defaultDateFormat];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm z"];
    
    
    [self.imageController dismissViewControllerAnimated:TRUE completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImage *imageScaled = [self scaleImageToPixelCount:3000000 originalImage:image];
    NSDateFormatter *fullStyle=[DateFormat defaultDateFormat];
    [fullStyle setDateFormat:@"MM/dd/yyyy HH:mm z"];
    
    NSString *text=[NSString stringWithFormat:@"%@\n%@", [fullStyle stringFromDate:[NSDate date]], _editor.unitNumber];
    
    imageScaled=[self drawText:text inImage:imageScaled atPoint:CGPointMake(0, 0)];

    
    NSData *imageScaledData = UIImageJPEGRepresentation(imageScaled,0.6);
    //NSDate *date=[NSDate date];
    

    imageScaled = [self scaleImageToPixelCount:100000 originalImage:image];
    NSData *imageThumbData = UIImageJPEGRepresentation(imageScaled,0.8);

    
    BOOL isOn = tempDamage;
    [[contentData objectAtIndex:currentPhotoTag-1] setValue:(isOn ? @"yes" : @"no") forKey:@"Damage"];

//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld.jpg", currentPhotoTag]];
//    [[contentData objectAtIndex:currentPhotoTag-1] setValue:savedImagePath forKey:@"Thumbnail"];
    
    PhotoEntity *photo= [[DBHelper sharedInstance] insertPhotoWithCaseEntity:dbCaseEntry
                                             beforeAfter:_editor.beforeAfter > 0 ? @1 : @0
                                           andCaseNumber:_editor.nextCaseNumber
                                          currentPhotoId:currentPhotoTag
                                           formattedDate:[dateFormatter stringFromDate:[NSDate date]]
                                            seeSomething:isOn
                                               photoData:imageScaledData
                                           thumbnailData:imageThumbData];
    
    //[imageData writeToFile:savedImagePath atomically:NO];
    //[[contentData objectAtIndex:currentPhotoTag-1] setValue:savedImagePath forKey:@"FilePath"];
    [[contentData objectAtIndex:currentPhotoTag-1] setValue:@YES forKey:@"ImageSet"];
    
    
    
    
    [self updatePhotoThumb:currentPhotoTag withImage:photo];
    
    if (currentPhotoTag>photoVersion && currentPhotoTag < contentData.count && currentPhotoTag == totalNumberOfPhotos)
        [self addThumbnail];
    
}

-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{
    
    UIFont *font = [UIFont boldSystemFontOfSize:40];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(image.size.width-500, image.size.height-130,500,130);
    
   // [[UIColor whiteColor] set];
    //t:CGRectIntegral(rect) withFont:font];
    [text drawInRect:(rect) withAttributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName:[UIColor redColor]}];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(UIImage *)scaleImageToPixelCount:(long)pixelCount originalImage:(UIImage *)originalImage{
    double scaleFactor=sqrt(pixelCount/(originalImage.size.width*originalImage.size.height));
    long newWidth=scaleFactor*originalImage.size.width;
    newWidth=newWidth- (newWidth % 8);
    long newHeight=scaleFactor*originalImage.size.height;
    newHeight=newHeight- (newHeight % 8);
    
    return [PictureSetViewController imageWithImage:originalImage scaledToSize:CGSizeMake(newWidth, newHeight)];
}

+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (void)tempDamageChanged:(UISwitch *)sender forTag:(long)tag{
    damageSelection = sender.isOn;
    photoIndexDamage = tag;
}

- (void)damageChanged:(UISwitch *)sender forTag:(long)tag{
//    damageSelection = sender.isOn;
//    photoIndexDamage = tag;
    
   // tempDamage = sender.isOn;
    BOOL isOn = sender.isOn;
    
    [self updateDamage:isOn forTag:tag];

    
}

-(void)updateDamage:(BOOL)isDamage forTag:(long)tag{
    if(tag>0){
        PhotoEntity *photo=[[DBHelper sharedInstance] getPhotoForCase:dbCaseEntry andcurrentPhotoId:tag];
        if(photo){
            photo.seeSomething = [NSNumber numberWithBool:isDamage];
            [APPDELEGATE saveContext];
        }
        [[contentData objectAtIndex:(tag-1)] setValue:(isDamage ? @"yes" : @"no") forKey:@"Damage"];
        UIImageView *damageImg = (UIImageView *)[_scrollView viewWithTag:(200+tag)];
        if(damageImg)
            [damageImg setImage: isDamage ? [UIImage imageNamed:@"red_flag"] :nil];
    }
}

- (void)updatePhotoThumb:(long)tag withImage:(PhotoEntity *)photo{
    [self updateDamage:damageSelection forTag:photoIndexDamage];
    //NSString *imgPath = [[contentData objectAtIndex:tag-1] valueForKey:@"Thumbnail"];
    UIImageView *placeholderImage = (UIImageView *)[_scrollView viewWithTag:(100+currentPhotoTag)];
    BOOL hasImage=NO;
    if(photo.thumbnail){
        placeholderImage.image = [UIImage imageWithData:photo.thumbnail];
        hasImage = YES;
    } else {
        placeholderImage.image = nil;
    }
    
    //NSString *damage = [[photo.seeSomething objectAtIndex:tag-1] valueForKey:@"Damage"];
    UIImageView *damageImg = (UIImageView *)[_scrollView viewWithTag:(200+tag)];
    if(hasImage && ([photo.seeSomething boolValue] == YES)){
        [damageImg setImage:[UIImage imageNamed:@"red_flag"]];
    }
    else
        [damageImg setImage:nil];
    
    NSString *titleNew = [[contentData objectAtIndex:tag-1] valueForKey:@"Title1"];
    if(titleNew.length){
        UILabel *titleLabel = (UILabel *)[_scrollView viewWithTag:(300+currentPhotoTag)];
        [titleLabel setText:titleNew];
    }
}


- (IBAction)doneSelected:(id)sender{
    if(_editor.beforeAfter==0){
        [self displayCDWForm];
    } else {
        [self doFinishWork:NO];
    }
 /*
    //String fileName = kejs.newCaseNumber+"/"+kejs.beforeAfter+"/"+foto.photoId+".jpg";
    NSString *fileName = [NSString stringWithFormat:@"%ld/%ld/%@" ,_editor.newCaseNumber, _editor.beforeAfter, @"1.jpg"];
    NSString *filePath=[[contentData objectAtIndex:1] objectForKey: @"FilePath"];
    fileName = [NSString stringWithFormat:@"8081/0/%@", @"10.jpg"];

    NSLog(@"%@",fileName);
    
        NSString *accessKeyId = @"AKIAJ3Q53UQGESCRIHNQ";
        NSString *secretAccessKey = @"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";
        NSString *bucket = @"damagecase";

    
        AFAmazonS3Manager *s3Manager = [[AFAmazonS3Manager alloc] initWithAccessKeyID:accessKeyId secret:secretAccessKey];
        s3Manager.requestSerializer.region = AFAmazonS3USStandardRegion;
        s3Manager.requestSerializer.bucket = bucket;
    
        NSString *destinationPath = fileName;
    
//        [s3Manager postObjectWithFile:filePath
//                      destinationPath:destinationPath
//                           parameters:nil
//                             progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//                                 NSLog(@"%f%% Uploaded", (totalBytesWritten / (totalBytesExpectedToWrite * 1.0f) * 100));
//                             }
//                              success:^(AFAmazonS3ResponseObject *responseObject) {
//                                  NSLog(@"Upload Complete: %@", responseObject.URL);
//                              }
//                              failure:^(NSError *error) {
//                                  NSLog(@"Error: %@", error);
//                              }];
    
    [s3Manager putObjectWithFile:filePath
                 destinationPath:destinationPath
                      parameters:nil
                    progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                        NSLog(@"%f%% Uploaded", (totalBytesWritten / (totalBytesExpectedToWrite * 1.0f) * 100));

                      } success:^(AFAmazonS3ResponseObject *responseObject) {
                          NSLog(@"Upload Complete: %@", responseObject.URL);
                      } failure:^(NSError *error) {
                          NSLog(@"Error: %@", error);
                      }];
    */
    
}
-(void)displayCDWForm{
//    UIActionSheet *CDWSheet = [UIActionSheet sheetWithTitle:@"Customer Acknowledgement"];
//    [CDWSheet addButtonWithTitle:@"Accept CDW" handler:^{
//        [self doFinishWork:YES];
//    }];
//    [CDWSheet setDestructiveButtonWithTitle:@"Decline CDW" handler:^{
//        [self doFinishWork:NO];
//    }];
//    [CDWSheet setCancelButtonWithTitle:@"Cancel" handler:^{
//        //do nothing
//    }];
//
//    [CDWSheet showInView:self.view];
    _viewCDW.hidden=NO;
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _viewCDW.alpha=1;
                         CGRect frame=_viewCDW.frame;
                         frame.origin.y=0;
                         _viewCDW.frame=frame;
                     }
                     completion:^(BOOL finished){
                         // Wait one second and then fade in the view
                         
                     }];
    

}

-(IBAction)acceptCDW:(id)sender{
    //_viewCDW.hidden=YES;
    [self doFinishWork:YES];
    [self hideCDW];
}

-(IBAction)declineCDW:(id)sender{
    //_viewCDW.hidden=YES;
    [self doFinishWork:NO];
    [self hideCDW];
}

-(void)hideCDW{
    
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _viewCDW.alpha=0;
                         CGRect frame=_viewCDW.frame;
                         frame.origin.y=1000;
                         _viewCDW.frame=frame;
                     }
                     completion:^(BOOL finished){
                         // Wait one second and then fade in the view
                         _viewCDW.hidden=YES;
                     }];
    
    

}

//+json create case
- (void)doFinishWork:(BOOL)insurance{

    if(_editor.isNewCase){
        //TODO create indicator progress : "In Progress","Saving New Case");
        [self createCase:insurance];
    } else {
        [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOS7Style new];
        [JCNotificationCenter enqueueNotificationWithTitle:@"Case is being processed" message:nil tapHandler:^{ }];
        

        BOOL takenAfterButNotYetUploaded = [[DBHelper sharedInstance] checkIfCaseTaken:_editor.nextCaseNumber andBeforeAfter:1];
        BOOL photoMin=1;
        //photoAfter >= photoMin
        if(photoMin <= _editor.photosAfter || takenAfterButNotYetUploaded){
            [self createCase:insurance];
        } else {
            NSLog(@"will this ever happen???");
            dbCaseEntry.caseNumber = [NSNumber numberWithLong: _editor.caseNumber];
            dbCaseEntry.nextCaseNumber = [NSNumber numberWithLong: _editor.nextCaseNumber];
            dbCaseEntry.beforeAfter = _editor.beforeAfter > 0 ? @1 : @0;
            dbCaseEntry.licencePlate = _editor.licensePlate;
            dbCaseEntry.model = _editor.model;
            dbCaseEntry.insurance = insurance ? @1 : @0;
            dbCaseEntry.accessKey = [[ApiClient sharedInstance] tokenKey];
            dbCaseEntry.status=@2; //readyForSync
            [APPDELEGATE saveContext];
            
//            [[DBHelper sharedInstance] insertCaseWithCaseNumber: _editor.caseNumber
//                                                 nextCaseNumber: _editor.nextCaseNumber
//                                                    beforeAfter: _editor.beforeAfter > 0 ? @1 : @0
//                                                   licensePlate: _editor.licensePlate
//                                                          model: _editor.model
//                                                      insurance: insurance ? @1 : @0
//                                                      accessKey: [[ApiClient sharedInstance] tokenKey]];
            //: dbHelper.insertCase(caseNumber, newCaseNumber, beforeAfter, licensePlate, model, insurance, accessKey);
            //TODO start sync
            [self popToLicence];
            [[ApiClient sharedInstance] startSync];
        }
    }
}

-(void)createCase:(BOOL)insurance{
    NSDictionary *dict=[[NSDictionary alloc] initWithObjectsAndKeys: _editor.unitNumber, @"unitNumber", nil];
    
    [[ApiClient sharedInstance] requestPostCase:dict data:@1 onSuccess:^(CaseCreateModel *nextCase) {
        
        NSLog(@"%ld", nextCase.caseNumber);
        /**
         * Successfully created the case
         */
        if(nextCase.msg == 2){
            //
            /*
             String newCaseNumber = responseCreateCase.getString("caseNumber");
             createResponse.put("status", "3");
             createResponse.put("newCaseNumber", newCaseNumber);
             createResponse.put("unitNumber", unitNumber);
             createResponse.put("licensePlate", licensePlate);
             createResponse.put("model", model);
             */
        } else if (nextCase.msg == 3 || nextCase.msg == 4){
            //žžž   ??????
            /*
             createResponse = new JSONObject();
             createResponse.put("status", "4");
             createResponse.put("message", "Please contact system administrator.");
             */
        }
        
        dbCaseEntry.caseNumber = [NSNumber numberWithLong: _editor.caseNumber];
        dbCaseEntry.nextCaseNumber = [NSNumber numberWithLong: nextCase.caseNumber];
        dbCaseEntry.beforeAfter = _editor.beforeAfter > 0 ? @1 : @0;
        dbCaseEntry.licencePlate = _editor.licensePlate;
        dbCaseEntry.model = _editor.model;
        dbCaseEntry.insurance = insurance ? @1 : @0;
        dbCaseEntry.accessKey = [[ApiClient sharedInstance] tokenKey];
        dbCaseEntry.status = @2;
        [APPDELEGATE saveContext];
        

        
//        CaseEntity *dbEntity=[NSEntityDescription insertNewObjectForEntityForName:@"CaseEntity" inManagedObjectContext:context];
//        
//        dbEntity.caseNumber = [NSNumber numberWithLong:_editor.caseNumber];
//        dbEntity.nextCaseNumber = [NSNumber numberWithLong:nextCase.caseNumber];
//        dbEntity.beforeAfter = _editor.beforeAfter > 0 ? @1 : @0;
//        dbEntity.licencePlate = _editor.licensePlate;
//        dbEntity.model = _editor.model;
//        dbEntity.insurance = insurance ? @1 : @0;
//        dbEntity.accessKey = [[ApiClient sharedInstance] tokenKey];
//        
//        [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
        
        /*
         String newCaseNumber = createResponse.getString("newCaseNumber");
         String caseNumber = preferences.getString("caseNumber", "-1");
         String beforeAfter = preferences.getString("beforeAfter", "0");
         */
        //: dbHelper.insertCase(caseNumber, newCaseNumber, beforeAfter, licensePlate, model, insurance, accessKey);
        
                [self popToLicence];
        [[ApiClient sharedInstance] startSync];
    } onFailure:^(NSError *error) {
        //NOK
                [self popToLicence];
    }];
}

- (void)deleteCase{
    //deleted on startup...
}

- (void)dealloc{
    [self removePhotoObservers];
}

@end
