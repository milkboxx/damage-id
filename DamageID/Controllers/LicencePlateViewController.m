//
//  LicencePlateViewController.m
//  DamageID
//
//  Created by Davor Galambos on 19/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "LicencePlateViewController.h"
#import "CaseModel.h"
#import "ApiClient.h"
#import "AFAmazonS3Manager.h"
#import "AFAmazonS3RequestSerializer.h"
#import "AFAmazonS3ResponseSerializer.h"
#import "ConfirmationViewController.h"
#import "PictureSetViewController.h"
#import "AppDelegate.h"
#import "CaseEntity.h"
#import "PhotoEntity.h"
#import "LoginViewController.h"

@interface LicencePlateViewController (){
    long caseId;
}

@end

@implementation LicencePlateViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _lblVersion.text = [NSString stringWithFormat:@"Version:\n%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    UIImage *imgButton=[[UIImage imageNamed:@"start_button"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4) resizingMode:UIImageResizingModeStretch];
    [_btnStart setBackgroundImage:imgButton forState:UIControlStateNormal];
    [_btnLogout setBackgroundImage:imgButton forState:UIControlStateNormal];
    [_retryButton setBackgroundImage:imgButton forState:UIControlStateNormal];
    //_txtfUnitNumber.text=@"9999";

    
    caseId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"internalCaseId"] longValue];
    caseId+=1;
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithLong:caseId] forKey:@"internalCaseId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkIfUnsyncedItems:) name:@"refreshRetryButton" object:nil];

    [self observeKeyboardNotifications];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self checkIfUnsyncedItems:nil];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshRetryButton" object:nil];
    [self stopObervingKeyboard];
}

-(void)checkIfUnsyncedItems:(NSNotification *)notification{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CaseEntity" inManagedObjectContext:[APPDELEGATE managedObjectContext]];
    [fetchRequest setEntity:entity];
    //[fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"status == 5"]];
    NSArray *fetchedObjects = [[APPDELEGATE managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    BOOL isHidden=YES;
    for(CaseEntity *object in fetchedObjects){
        NSLog(@"%d", [object.status intValue]);
        if([object.status intValue] == 1 || [object.status intValue] ==4 || [object.status intValue] == 2){
            if([object.status intValue] == 1){
                for (PhotoEntity *photo in object.photo) {
                    if ([photo.status intValue] != 3 && [photo.status intValue] != 5 && [photo.status intValue] != 0) {
                        isHidden = NO;
                    }
                }
            } else {
                isHidden = NO;
            }
        }
    }
    _retryButton.hidden = isHidden;
    _retryLabel.hidden = isHidden;
}

-(IBAction)retrySync:(id)sender{
    _retryButton.hidden = YES;
    _retryLabel.hidden = YES;
    [[ApiClient sharedInstance] startSync];
}

-(IBAction)logout:(id)sender{
    self.loginVC.txtfPassword.text = @"";
    self.loginVC.txtfUsername.text = @"";
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _txtfUnitNumber.text=@"";
    _txtfLicensePlate.text=@"";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
        [self.navigationController setNavigationBarHidden:NO];
    _scrollView.contentSize = self.view.bounds.size;
    _imgBackground.frame=CGRectMake(0, -64, self.view.bounds.size.width, self.view.bounds.size.height+64);

    CGRect frame = _lblVersion.frame;
    frame.origin.y = self.view.bounds.size.height-10-_lblVersion.frame.size.height;
    _lblVersion.frame = frame;

}


#pragma Keyboard methods
- (void)observeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)stopObervingKeyboard {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)keyboardWillShow:(NSNotification *)notification {
    float kbHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    CGRect frame=_scrollView.frame;
    frame.size.height=self.view.bounds.size.height-kbHeight;
    _scrollView.frame=frame;
}


- (void)keyboardWillHide:(NSNotification *)notification {
    CGRect frame=_scrollView.frame;
    frame.size.height=self.view.bounds.size.height;
    _scrollView.frame=frame;
    
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self openCase];
    
    return YES;
}


#pragma Actions

- (IBAction)btnSignInPressed:(id)sender{
    [_txtfLicensePlate resignFirstResponder];
    [_txtfUnitNumber resignFirstResponder];
    [self openCase];
}


- (void)openCase{
    [self btnSignInSetSigningIn:YES];
    [[ApiClient sharedInstance] requestCaseForUnitNumber:_txtfUnitNumber.text orLicencePlate:_txtfLicensePlate.text onSuccess:^(CaseModel *result) {
        [self btnSignInSetSigningIn:NO];
        [self processResponse: result];
    } onFailure:^(NSError *error) {
        [self btnSignInSetSigningIn:NO];
    }];
}

-(void)btnSignInSetSigningIn:(BOOL)isSigning{
    _btnStart.enabled = !isSigning;
    [_btnStart setTitle: isSigning ? @"STARTING..." : @"START" forState:UIControlStateNormal];
    [_btnStart setEnabled:!isSigning];
}

-(void)processResponse:(CaseModel *)model{
    BOOL takenAfterButNotYetUploaded = NO;
    CaseModel *editor = [[CaseModel alloc] init];
    editor.caseNumber = caseId;
    
    /**
     * Success message
     * @isNewCase value is not new case
     */
    if (model.msg == 4) {
        {
            editor.unitNumber = model.unitNumber;
            editor.licensePlate = model.licensePlate;
            editor.model = model.model;
            editor.isNewCase = NO;
            editor.nextCaseNumber = model.caseNumber;
            editor.thumbnailUrl = model.thumbnailUrl;
            editor.carId = model.carId;
        }
        long photosMin = 1;
        
        /**
         * If the number of photos for After is higher than 1 OR case status in upload, incomplete or archived status OR taken but isn't uploaded
         * If it isn't the case has before photos, and redirect to Checkin activity
         */
        if (photosMin <= model.photosAfter || model.status >= 4 || takenAfterButNotYetUploaded == YES){
            if(model.status == 15){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Previous case for this unit is archived" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            //pcture set
            editor.beforeAfter = 0;
            editor.isNewCase = YES;
            [self openPictureSet:editor];
        } else {
            editor.photosBefore = model.photosBefore;
            editor.photosAfter = model.photosAfter;
            
            [self openEditor:editor];
        }
    }

    
    /**
     * Success message
     * @isNewCase value is new case
     */
    else if (model.msg == 3) {
        editor.unitNumber = model.unitNumber;
        editor.licensePlate = model.licensePlate;
        editor.model = model.model;
        editor.isNewCase = YES;
        editor.thumbnailUrl = model.thumbnailUrl;
        editor.carId = model.carId;
        editor.beforeAfter = 0;
        
        [self openPictureSet:editor];
    }
    
    //mySQL exception
    else if (model.msg == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please contact system administrator." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (model.msg == 5) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"That car is not in the database." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (model.msg == 6) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"That car is currently archived." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if (model.msg == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"UnitNumber Or LicensePlate not provided." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Something went wrong. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)openEditor:(CaseModel *)model{
    ConfirmationViewController *vc = [[ConfirmationViewController alloc] initWithNibName:XIB(@"ConfirmationViewController") bundle:nil];
    vc.editor = model;
    [self.navigationController pushViewController:vc animated:YES];
    
//    NSString *accessKeyId = @"AKIAJ3Q53UQGESCRIHNQ";
//    NSString *secretAccessKey = @"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";
//    NSString *bucket = @"damagecase";
//
//    
//    
//    AFAmazonS3Manager *s3Manager = [[AFAmazonS3Manager alloc] initWithAccessKeyID:accessKeyId secret:secretAccessKey];
//    s3Manager.requestSerializer.region = AFAmazonS3USWest1Region;
//    s3Manager.requestSerializer.bucket = bucket;
//    
//    NSString *destinationPath = @"/pathOnS3/to/file.txt";
//    
//    [s3Manager postObjectWithFile:@"/path/to/file.txt"
//                  destinationPath:destinationPath
//                       parameters:nil
//                         progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//                             NSLog(@"%f%% Uploaded", (totalBytesWritten / (totalBytesExpectedToWrite * 1.0f) * 100));
//                         }
//                          success:^(AFAmazonS3ResponseObject *responseObject) {
//                              NSLog(@"Upload Complete: %@", responseObject.URL);
//                          }
//                          failure:^(NSError *error) {
//                              NSLog(@"Error: %@", error);
//                          }];
}

-(void)openPictureSet:(CaseModel *)model{
    PictureSetViewController *vc=[[PictureSetViewController alloc] initWithNibName:@"PictureSetViewController" bundle:nil];
    vc.editor = model;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
