//
//  ConfirmationViewController.h
//  DamageID
//
//  Created by Davor Galambos on 23/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CaseModel;
@interface ConfirmationViewController : UIViewController{
    
    
}

@property (nonatomic, weak) IBOutlet UILabel *lblModel;
@property (nonatomic, weak) IBOutlet UILabel *lblUnitNumber;
@property (nonatomic, weak) IBOutlet UIButton *btnRadioReturn;
@property (nonatomic, weak) IBOutlet UIButton *btnRadioNewCase;
@property (nonatomic, weak) IBOutlet UIButton *btnContinue;
@property (nonatomic, strong) CaseModel *editor;
@end
