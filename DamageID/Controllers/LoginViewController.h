//
//  LoginViewController.h
//  DamageID
//
//  Created by Davor Galambos on 08/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, assign) IBOutlet UIButton *btnSignIn;
@property (nonatomic, assign) IBOutlet UIScrollView *scrollView;
@property (nonatomic, assign) IBOutlet UIImageView *imgBackground;
@property (nonatomic, assign) IBOutlet UITextField *txtfUsername;
@property (nonatomic, assign) IBOutlet UITextField *txtfPassword;

- (IBAction)btnSignInPressed:(id)sender;

@end
