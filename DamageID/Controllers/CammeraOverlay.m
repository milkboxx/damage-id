//
//  CammeraOverlay.m
//  DamageID
//
//  Created by Davor Galambos on 27/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "CammeraOverlay.h"

@implementation CammeraOverlay

- (void)awakeFromNib{
    [_swDamage setOnTintColor:AccentRedColor];
    _swDamage.transform = CGAffineTransformMakeRotation(90.0*M_PI/180.0);
    _lblDamage.transform = CGAffineTransformMakeRotation(90.0*M_PI/180.0);
    [_swDamage addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];

}

- (void)afterInit{
    _imgOverlay.image = [UIImage imageNamed:_overlayImgName];
    [_swDamage setOn:_initialDamage];
    [self switchChanged:_swDamage];
}

- (void)switchChanged:(id)sender{
    [_delegate tempDamageChanged:sender forTag:_imageTag];
     //<#(UISwitch *)#> forTag:<#(long)#>:sender forTag:_imageTag];
}

-(void)hideDamageSwitch{
    [_lblDamage setHidden:YES];
    [_swDamage setHidden:YES];
}

@end
