//
//  AppDelegate.h
//  DamageID
//
//  Created by Davor Galambos on 08/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
@class LicencePlateViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *rootViewController;
@property (nonatomic, assign) BOOL fullScreenVideo;
@property (nonatomic, assign) long photoVersion;
//@property (nonatomic, assign) LicencePlateViewController *licenceController;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

