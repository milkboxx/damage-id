//
//  ApiClient.h
//  DamageID
//
//  Created by Davor Galambos on 18/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <Foundation/Foundation.h>
@class LoginModel, CaseModel, CaseCreateModel;

@interface ApiClient : NSObject

+ (id)sharedInstance;
- (void)logInForUsername:(NSString *)username andPassword:(NSString *)password onSuccess:(void (^)(LoginModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure;
- (void)updateAccessKey:(NSString *)key;
- (void)requestPostCase:(NSDictionary *)data data:(id)info onSuccess:(void (^)(CaseCreateModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure;

- (void)requestCaseForUnitNumber:(NSString *)unitNumber orLicencePlate:(NSString *)licencePlate onSuccess:(void (^)(CaseModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure;
//- (void)requestPostCase:(NSString *)unitNumber data:(NSDictionary *)data onSuccess:(void (^)(CaseModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure;
- (void)startSync;
- (void)syncPhotos;
@property (nonatomic, strong) NSString *tokenKey;
@end
