//
//  DateFormat.h
//  iPhone
//
//  Created by Davor Galambos on 03/07/15.
//
//

#import <Foundation/Foundation.h>

@interface DateFormat : NSObject

+ (NSDateFormatter *)defaultDateFormat;

@end
