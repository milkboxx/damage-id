//
//  DBHelper.h
//  DamageID
//
//  Created by Davor Galambos on 14/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class CaseEntity, PhotoEntity;
@interface DBHelper : NSObject

@property (nonatomic, strong) NSManagedObjectContext *context;
+ (id)sharedInstance;
- (BOOL)checkIfCaseTaken:(long)caseNumber andBeforeAfter:(long)beforeAfter;
- (CaseEntity *)insertCaseWithCaseNumber:(long)caseNumber nextCaseNumber:(long)nextCaseNumber beforeAfter:(NSNumber *)beforeAfter licensePlate:(NSString *)licensePlate model:(NSString *)model insurance:(NSNumber *)insurance accessKey:(NSString *)accessKey;
- (PhotoEntity *)insertPhotoWithCaseEntity:(CaseEntity *)caseEntity beforeAfter:(NSNumber *)beforeAfter andCaseNumber:(long)caseNumber currentPhotoId:(long)currentPhotoId formattedDate:(NSString *)format seeSomething:(BOOL)seeSomething photoData:(NSData *)photoData thumbnailData:(NSData *)thumbnailData;
- (PhotoEntity *)getPhotoForCase:(CaseEntity *)caseEntity andcurrentPhotoId:(long)currentPhotoId;


@end
