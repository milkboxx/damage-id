//
//  DBHelper.m
//  DamageID
//
//  Created by Davor Galambos on 14/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "DBHelper.h"
#import "AppDelegate.h"
#import "CaseEntity.h"
#import "PhotoEntity.h"

@implementation DBHelper

+ (id)sharedInstance {
    static DBHelper *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[DBHelper alloc] init];
        
    });
    
    return __sharedInstance;
}

- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    self.context = delegate.managedObjectContext;
    
    return self;
}


//This is part of code from andorid app and doesnt fit in ios app.
- (BOOL)checkIfCaseTaken:(long)caseNumber andBeforeAfter:(long)beforeAfter{
    return NO;
//    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"CaseEntity" inManagedObjectContext:_context];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:entityDescription];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nextCaseNumber == %ld AND beforeAfter == %ld", caseNumber, beforeAfter];
//    [request setPredicate:predicate];
//    NSError *error;
//    CaseEntity *syncItem = [[_context executeFetchRequest:request error:&error] lastObject];
//
//    
//    if(syncItem)
//        return YES;
//    return NO;
}

-(CaseEntity *)insertCaseWithCaseNumber:(long)caseNumber nextCaseNumber:(long)nextCaseNumber beforeAfter:(NSNumber *)beforeAfter licensePlate:(NSString *)licensePlate model:(NSString *)model insurance:(NSNumber *)insurance accessKey:(NSString *)accessKey{
    CaseEntity *dbEntity=[NSEntityDescription insertNewObjectForEntityForName:@"CaseEntity" inManagedObjectContext:_context];
    
    dbEntity.caseNumber = [NSNumber numberWithLong:caseNumber];
    dbEntity.nextCaseNumber = [NSNumber numberWithLong:nextCaseNumber];
    dbEntity.beforeAfter = beforeAfter;
    dbEntity.licencePlate = licensePlate;
    dbEntity.model = model;
    dbEntity.insurance = insurance;
    dbEntity.accessKey = accessKey;
    dbEntity.status = @3; //draft
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    
    
    return dbEntity;
}

- (PhotoEntity *)insertPhotoWithCaseEntity:(CaseEntity *)caseEntity beforeAfter:(NSNumber *)beforeAfter andCaseNumber:(long)caseNumber currentPhotoId:(long)currentPhotoId formattedDate:(NSString *)format seeSomething:(BOOL)seeSomething photoData:(NSData *)photoData thumbnailData:(NSData *)thumbnailData{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentPhotoId == %ld", currentPhotoId];
    PhotoEntity *dbEntity = [caseEntity.photo filteredSetUsingPredicate:predicate].anyObject;
    
    if(!dbEntity)
        dbEntity = [NSEntityDescription insertNewObjectForEntityForName:@"PhotoEntity" inManagedObjectContext:_context];
    [caseEntity addPhotoObject:dbEntity];

    dbEntity.beforeAfter = beforeAfter;
    dbEntity.caseNumber = [NSNumber numberWithLong:caseNumber];
    dbEntity.currentPhotoId = [NSNumber numberWithLong:currentPhotoId];
    dbEntity.format = format;
    dbEntity.photo = photoData;
    dbEntity.seeSomething = [NSNumber numberWithBool:seeSomething];
    dbEntity.thumbnail = thumbnailData;
    dbEntity.imageRemoteId = [self photoTranslatorForPhoto:currentPhotoId];
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    
    return dbEntity;
}

- (PhotoEntity *)getPhotoForCase:(CaseEntity *)caseEntity andcurrentPhotoId:(long)currentPhotoId{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"currentPhotoId == %ld", currentPhotoId];
    PhotoEntity *dbEntity = [caseEntity.photo filteredSetUsingPredicate:predicate].anyObject;
    return dbEntity;
}

-(NSNumber *)photoTranslatorForPhoto:(long)photoId{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    long photoVersion = delegate.photoVersion;
    
    if(photoVersion==5){
        if(photoId==1) return @1;
        if(photoId==2) return @2;
        if(photoId>2) return [NSNumber numberWithLong:photoId+1];
    }
    if(photoVersion==7){
        if(photoId==1) return @6;
        if(photoId==2) return @0;
        if(photoId==3) return @1;
        if(photoId==4) return @2;
        if(photoId==5) return @3;
        if(photoId==6) return @4;
        if(photoId==7) return @5;
        if(photoId>7) return [NSNumber numberWithLong:photoId-1];
    }
    return @99;
}

@end
