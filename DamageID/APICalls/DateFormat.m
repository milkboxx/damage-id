//
//  DateFormat.m
//  iPhone
//
//  Created by Davor Galambos on 03/07/15.
//
//

#import "DateFormat.h"

@implementation DateFormat

+ (NSDateFormatter *)defaultDateFormat{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    NSString *currentLocale = [NSLocale currentLocale].localeIdentifier;
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:currentLocale]];
    return dateFormatter;
}

@end
