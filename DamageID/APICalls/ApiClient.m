//
//  ApiClient.m
//  DamageID
//
//  Created by Davor Galambos on 18/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "ApiClient.h"
#import "AFNetworking.h"
#import "LoginModel.h"
#import "CaseModel.h"
#import "CaseCreateModel.h"
#import "CaseEntity.h"
#import "PhotoEntity.h"
#import "AppDelegate.h"
#import "AFAmazonS3Manager.h"
#import "AFAmazonS3RequestSerializer.h"
#import "AFAmazonS3ResponseSerializer.h"
#import "JCNotificationCenter.h"

#define kDMG_BASE_URL @"https://app.damageid.com"

@implementation ApiClient{
    NSMutableArray *photosInSync;
    long uploadedImagesCount;
}

+ (id)sharedInstance {
    static ApiClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[ApiClient alloc] init];
        [__sharedInstance afterInit];

    });
    
    return __sharedInstance;
}

-(void)afterInit{
    photosInSync=[[NSMutableArray alloc] init];
    uploadedImagesCount = 0;
}


- (void)startSync{
    NSLog(@"Starting sync");
    NSError *error;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = delegate.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CaseEntity" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];

    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"status == 2 || status == 4"]];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for(CaseEntity *object in fetchedObjects){
        
        if([object.beforeAfter intValue]==1){
            object.status = @5;
            NSDictionary *dataDict=[NSDictionary dictionaryWithObjectsAndKeys:@"4",@"status", nil];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:&error];
                NSLog(@"Starting sync update status wsr");
            [self doWSrequest:jsonData andMethod:[@"api/case/updateStatus/" stringByAppendingString:[NSString stringWithFormat:@"%d", [object.nextCaseNumber intValue]]] andCaseNumber:[object.nextCaseNumber intValue] andRequest:YES tokenKey:object.accessKey onSuccess:^(id result) {
                object.status = @1;
                [delegate saveContext];
                [self syncPhotos];
                NSLog(@"update status wsr OK");
                
            } onFailure:^(NSError *error) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                object.status = @4;
                [delegate saveContext];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRetryButton" object:nil];

                NSLog(@"FAILED updae status wsr");
                //
            }];
            
            
            // jResponseData = doWSRequest(jRequestData.toString(), "api/case/updateStatus/" + kejs.newCaseNumber, kejs.newCaseNumber, true, kejs.accessKey);

        } else if ([object.beforeAfter intValue]==0){
            if([object.insurance intValue] == 1){
                object.status = @5;
                NSDictionary *dataDict=[NSDictionary dictionaryWithObjectsAndKeys:@"1", @"insurance", nil];
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:&error];
                NSLog(@"Starting sync insurance");

                [self doWSrequest:jsonData andMethod:[@"api/case/insure/" stringByAppendingString:[NSString stringWithFormat:@"%d", [object.nextCaseNumber intValue]]] andCaseNumber:[object.nextCaseNumber intValue] andRequest:YES tokenKey:object.accessKey onSuccess:^(id result) {
                    //
                    object.status = @1;
                    [delegate saveContext];
                    [self syncPhotos];
                    NSLog(@"insurance received stauts OK");
                } onFailure:^(NSError *error) {
                    object.status = @4;
                    [delegate saveContext];
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    NSLog(@"FAILED insuranse  wsr");
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRetryButton" object:nil];

                }];
                
            }
            else{
                object.status = @1;
                [delegate saveContext];
                //[self syncPhotos];
            }
        }
        

    }
    [self syncPhotos];
}

-(void)syncPhotos{
    NSLog(@"Starting sync photos");
    NSError *error;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = delegate.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CaseEntity" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    

    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"status == 1"]];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for(CaseEntity *object in fetchedObjects){
        NSLog(@"%ld", [object.status longValue]);
        for(PhotoEntity *photoItem in object.photo){
            if(![photosInSync containsObject:photoItem] && [photoItem.status intValue] != 3 ){
                [photosInSync addObject:photoItem];
                //object.status = @15;
            }
            
        }
    }
    [self startUploadQue];
}

-(void)startUploadQue{
    [self startNetworkIndicator:NO];
    if(photosInSync.count>0){
        PhotoEntity *photoItem = [photosInSync objectAtIndex:0];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld_%ld.jpg",[photoItem.caseRelation.nextCaseNumber longValue], [photoItem.imageRemoteId longValue]]];
        NSString *serverImagePath = [NSString stringWithFormat:@"%ld.jpg", [photoItem.imageRemoteId longValue]];
        
        [photoItem.photo writeToFile:savedImagePath atomically:YES];
        
        NSString *accessKeyId = @"AKIAJ3Q53UQGESCRIHNQ";
        NSString *secretAccessKey = @"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";
        NSString *bucket = @"damagecase";
        
        
        AFAmazonS3Manager *s3Manager = [[AFAmazonS3Manager alloc] initWithAccessKeyID:accessKeyId secret:secretAccessKey];
        s3Manager.requestSerializer.region = AFAmazonS3USStandardRegion;
        s3Manager.requestSerializer.bucket = bucket;
        long caseNumber=[photoItem.caseRelation.nextCaseNumber longValue];
        NSString *destinationPath = [NSString stringWithFormat:@"%ld/%d/%@", caseNumber, [photoItem.beforeAfter intValue], serverImagePath];
        NSLog(@"Start uploading to s3: %@", destinationPath);
        [self startNetworkIndicator:YES];
        photoItem.status = @5;
        [s3Manager putObjectWithFile:savedImagePath
                     destinationPath:destinationPath
                          parameters:nil
                            progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                                
                            } success:^(AFAmazonS3ResponseObject *responseObject) {
                                NSLog(@"Upload Complete: %@", responseObject.URL);
                                photoItem.status = @3;
                                
                                
                                //delete success image
                                photoItem.photo=nil;
                                photoItem.thumbnail=nil;
                                [APPDELEGATE saveContext];
                                if([[NSFileManager defaultManager] fileExistsAtPath:savedImagePath]){
                                    [[NSFileManager defaultManager] removeItemAtPath:savedImagePath error:NULL];
                                }
                                
                                
                                [photosInSync removeObject:photoItem];
                                uploadedImagesCount++;
                                if(photosInSync.count>0)
                                    [self checkIfAllDone];
                                [self startUploadQue];
                                
                                
                            } failure:^(NSError *error) {
                                NSLog(@"Error: %@", error);
                                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                [alert show];
                                photoItem.status = @4;
                                [APPDELEGATE saveContext];
                                [photosInSync removeObject:photoItem];
                                uploadedImagesCount++;
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRetryButton" object:nil];

                                [self startUploadQue];
                            }];
    } else {
        [self startNetworkIndicator:NO];
        [self checkIfAllDone];
    }
}


-(void)checkIfAllDone{
    NSLog(@"Starting sync photos");
    NSError *error;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = delegate.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CaseEntity" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"status == 1"]];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for(CaseEntity *object in fetchedObjects){
        long count = object.photo.count;
        int countDone=0;
        int countError=0;
        int countOther=0;
        
        for(PhotoEntity *photoItem in object.photo){
            if([photoItem.status intValue]== 3)
                countDone++;
            else if([photoItem.status intValue]== 4)
                countError++;
            else
                countOther++;
        }
        NSLog(@"Status check for upload: done %d, error %d, outhe %d, total %ld", countDone, countError, countOther, count);
        //if(countDone > 0){
        if(countDone == count){
            [self notifyService:object allDone: countDone == count ? YES : NO];
        }
    }
}

-(void)notifyService:(CaseEntity *)item allDone:(BOOL)allDone{
    NSLog(@"Starting notifyService");
    NSError *error;
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
    [dataDict setObject:[NSString stringWithFormat:@"%d" , [item.beforeAfter intValue]] forKey:@"ba"];
    
    NSString *photosString = @"";
    NSString *seeSomethingPhotos = @"";
    NSString *partialPhotos = @"";
    for(PhotoEntity *photoItem in item.photo){
        if([photoItem.status intValue]==3)
            photosString = [photosString stringByAppendingFormat:@"%d,", [photoItem.imageRemoteId intValue]];
        else
            partialPhotos = [partialPhotos stringByAppendingFormat:@"%d,", [photoItem.imageRemoteId intValue]];
        
        if([photoItem.seeSomething boolValue]==YES)
            seeSomethingPhotos = [seeSomethingPhotos stringByAppendingFormat:@"%d,", [photoItem.imageRemoteId intValue]];
    }
    
    if(photosString.length>0)
        photosString=[photosString substringToIndex:photosString.length-1];
    if(seeSomethingPhotos.length>0)
        seeSomethingPhotos=[seeSomethingPhotos substringToIndex:seeSomethingPhotos.length-1];
    if(partialPhotos.length>0)
        partialPhotos=[partialPhotos substringToIndex:partialPhotos.length-1];
    
    
    [dataDict setObject:photosString forKey:@"photos"];
    
    
    [dataDict setObject:partialPhotos forKey:@"partial"];
    [dataDict setObject:seeSomethingPhotos forKey:@"seePhotos"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:&error];
    NSLog(@"data: %@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    
    [self doWSrequest:jsonData andMethod:[@"api/case/uploadPhotos/" stringByAppendingString:[NSString stringWithFormat:@"%d", [item.nextCaseNumber intValue]]] andCaseNumber:[item.nextCaseNumber intValue] andRequest:YES tokenKey:item.accessKey onSuccess:^(id result) {
        if(allDone==YES){
            NSLog(@"notifyServiceSuessful");
            [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOS7Style new];
            [JCNotificationCenter enqueueNotificationWithTitle:[NSString stringWithFormat:@"Case: %ld Upload complete" , [item.nextCaseNumber longValue]] message:nil tapHandler:^{ }];

            item.status=@0;
            [[APPDELEGATE managedObjectContext] deleteObject:item];
            [APPDELEGATE saveContext];
        }
        else{
            NSLog(@"partial upload notification...");
            
        }
        
    } onFailure:^(NSError *error) {
        //status = 1... OK
        //
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRetryButton" object:nil];

         NSLog(@"notifyServiceFailed");
    }];

}


- (void)updateAccessKey:(NSString *)key{
    _tokenKey = [NSString stringWithFormat:@"DID %@" ,key];
}

- (void)logInForUsername:(NSString *)username andPassword:(NSString *)password onSuccess:(void (^)(LoginModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure{
    NSMutableDictionary *bodyParams=[[NSMutableDictionary alloc] init];
    [bodyParams setObject:username forKey:@"damageuser"];
    [bodyParams setObject:password forKey:@"damagepass"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:bodyParams options:NSJSONWritingPrettyPrinted error:&error];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/api/authenticate", kDMG_BASE_URL];
    NSURL *url = [NSURL URLWithString:requestUrl];
    NSMutableURLRequest* request=[self loginPostRequestWithUrl:url];
    [request setHTTPBody:jsonData];
    
    [self startNetworkIndicator:YES];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self startNetworkIndicator:NO];
        NSError *error;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        if(error)
            onFailure(error);
        LoginModel *model=[[LoginModel alloc] initWithDictionary:dictionary];
        onSuccess(model);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self startNetworkIndicator:NO];
        onFailure(error);
    }];
    
    [operation start];
}

- (void)doWSrequest:(NSData *)requestObj andMethod:(NSString *)method andCaseNumber:(int)caseNumber andRequest:(BOOL)isRequestPut tokenKey:(NSString *)tokenKey onSuccess:(void (^)(CaseCreateModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/%@", kDMG_BASE_URL, method];
    NSLog(@"Sending requesr: %@", requestUrl);
    NSURL *url = [NSURL URLWithString:requestUrl];
    NSMutableURLRequest* request=[self defaultPostRequestWithUrl:url];
    [request setValue:@"X-Requested-With" forHTTPHeaderField:@"XMLHttpRequest"];
    [request setHTTPBody:requestObj];
    [request setValue:tokenKey forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:isRequestPut ? @"PUT": @"POST"];
    
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id dictionary) {
        [self startNetworkIndicator:NO];

        onSuccess(dictionary);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self startNetworkIndicator:NO];
        NSLog(@"Error: %@", [error localizedDescription]);
        onFailure(error);
        
    }];
    [op start];
    
    
    
    
       [self startNetworkIndicator:YES];
}




- (void)requestPostCase:(NSDictionary *)data data:(id)info onSuccess:(void (^)(CaseCreateModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/api/case/create", kDMG_BASE_URL];

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
    NSURL *url = [NSURL URLWithString:requestUrl];
    NSMutableURLRequest* request=[self defaultPostRequestWithUrl:url];
    [request setValue:@"X-Requested-With" forHTTPHeaderField:@"XMLHttpRequest"];
    [request setHTTPBody:jsonData];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id dictionary) {
        [self startNetworkIndicator:NO];
        CaseCreateModel *model=[[CaseCreateModel alloc] initWithDictionary:dictionary];

        onSuccess(model);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self startNetworkIndicator:NO];
        NSLog(@"Error: %@", [error localizedDescription]);
        onFailure(error);
        
    }];
    [op start];

    [self startNetworkIndicator:YES];
}

- (void)requestCaseForUnitNumber:(NSString *)unitNumber orLicencePlate:(NSString *)licencePlate onSuccess:(void (^)(CaseModel *result))onSuccess onFailure:(void(^)(NSError *error))onFailure{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/api/case/show", kDMG_BASE_URL];
    
    NSMutableDictionary *urlParams=[[NSMutableDictionary alloc] init];
    if(unitNumber.length > 0){
        [urlParams setObject:unitNumber forKey:@"unitNumber"];
    } else if (licencePlate.length > 0){
        [urlParams setObject:licencePlate forKey:@"licensePlate"];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter Unit ID or License." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        NSError *err;
        onFailure(err);
        return;

    }
    requestUrl = [requestUrl stringByAppendingString:[self formatedURLPathFromParams:urlParams]];
    
    AFJSONRequestSerializer *serial=[AFJSONRequestSerializer serializer];
    [serial setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [serial setValue:@"Close" forHTTPHeaderField:@"Connection"];
    [serial setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [serial setValue:_tokenKey forHTTPHeaderField:@"Authorization"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = serial;
    

    [self startNetworkIndicator:YES];

    [manager POST:requestUrl parameters:urlParams success:^(AFHTTPRequestOperation *operation, NSDictionary *dictionary) {
        [self startNetworkIndicator:NO];

        CaseModel *model=[[CaseModel alloc] initWithDictionary:dictionary];
        NSLog(@"case number: %ld", model.caseNumber);
        onSuccess(model);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"b");
    }];
    
}


- (NSString *)formatedURLPathFromParams: (NSDictionary *)params{
    NSArray *allKeys=[params allKeys];
    if(allKeys.count > 0){
        NSString *result = @"";
        for (NSString *key in allKeys) {
            result = [result stringByAppendingFormat:@"&%@=%@", key, [params objectForKey:key]];
        }
        result = [@"?" stringByAppendingString:[result substringFromIndex:1]];
        NSString *encodedString = [result stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        return encodedString;
    }
    return nil;
}


-(NSMutableURLRequest *)loginPostRequestWithUrl:(NSURL *)url{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:@"Close" forHTTPHeaderField:@"Connection"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    return request;
}

-(NSMutableURLRequest *)defaultPostRequestWithUrl:(NSURL *)url{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:@"Close" forHTTPHeaderField:@"Connection"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:_tokenKey forHTTPHeaderField:@"Authorization"];
    
    
    return request;
}

- (void)startNetworkIndicator:(BOOL)shouldStart{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:shouldStart];
}

@end
