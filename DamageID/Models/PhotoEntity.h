//
//  PhotoEntity.h
//  DamageID
//
//  Created by Davor Galambos on 26/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CaseEntity;

@interface PhotoEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * beforeAfter;
@property (nonatomic, retain) NSNumber * caseNumber;
@property (nonatomic, retain) NSNumber * currentPhotoId;
@property (nonatomic, retain) NSString * format;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSNumber * seeSomething;
@property (nonatomic, retain) NSData * thumbnail;
@property (nonatomic, retain) NSNumber * imageRemoteId;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) CaseEntity *caseRelation;

@end
