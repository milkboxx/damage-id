//
//  CaseCreateModel.h
//  DamageID
//
//  Created by Davor Galambos on 14/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface CaseCreateModel : BaseModel

@property (nonatomic, assign) long msg;
@property (nonatomic, assign) long caseNumber;
@property (nonatomic, assign) long insurance;
@property (nonatomic, strong) NSString *licensePlate;

- (CaseCreateModel *)initWithDictionary:(NSDictionary*)dictionary;


@end
