//
//  CaseModel.h
//  DamageID
//
//  Created by Davor Galambos on 20/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface CaseModel : BaseModel
@property (nonatomic, assign) long msg;
@property (nonatomic, assign) long caseNumber;
@property (nonatomic, assign) long status;
@property (nonatomic, strong) NSNumber *carId;
@property (nonatomic, strong) NSString *unitNumber;
@property (nonatomic, strong) NSString *licensePlate;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (nonatomic, assign) long photosBefore;
@property (nonatomic, assign) long photosAfter;

@property (nonatomic, assign) BOOL isNewCase;
@property (nonatomic, assign) long nextCaseNumber;
@property (nonatomic, assign) long beforeAfter;

- (CaseModel *)initWithDictionary:(NSDictionary*)dictionary;

    
@end
