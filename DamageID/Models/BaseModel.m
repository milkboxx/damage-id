//
//  BaseModel.m
//  DamageID
//
//  Created by Davor Galambos on 14/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

-(NSString *)parseString:(id)data{
    if ([[data class] isSubclassOfClass: [NSString class]]){
        return data;
    }
    return nil;
}

-(NSNumber *)parseStringToNumberLong:(id)inputStr{
    if([[inputStr class] isSubclassOfClass:[NSString class]]){
        NSScanner *scanner = [NSScanner scannerWithString:inputStr];
        NSInteger scannedValue;
        [scanner scanInteger:&scannedValue];
        return (scanner.isAtEnd ? [NSNumber numberWithInteger: scannedValue] : nil);
        
    } else if ([[inputStr class] isSubclassOfClass:[NSNumber class]]){
        return inputStr;
    }
    return nil;
}

@end
