//
//  LoginModel.m
//  DamageID
//
//  Created by Davor Galambos on 19/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

- (LoginModel *)initWithDictionary:(NSDictionary*)dictionary {
    if (self = [super init]) {
        self.accessKey = dictionary[@"accessKey"];
        self.fullName = dictionary[@"fullName"];
        self.photoVersion = [dictionary[@"photoVersion"] longValue];
        self.username = dictionary[@"username"];
    }
    return self;
}

@end
