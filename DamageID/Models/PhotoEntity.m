//
//  PhotoEntity.m
//  DamageID
//
//  Created by Davor Galambos on 26/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "PhotoEntity.h"
#import "CaseEntity.h"


@implementation PhotoEntity

@dynamic beforeAfter;
@dynamic caseNumber;
@dynamic currentPhotoId;
@dynamic format;
@dynamic photo;
@dynamic seeSomething;
@dynamic thumbnail;
@dynamic imageRemoteId;
@dynamic status;
@dynamic caseRelation;

@end
