//
//  CaseEntity.m
//  
//
//  Created by Davor Galambos on 23/07/15.
//
//

#import "CaseEntity.h"
#import "PhotoEntity.h"


@implementation CaseEntity

@dynamic accessKey;
@dynamic beforeAfter;
@dynamic caseNumber;
@dynamic insurance;
@dynamic licencePlate;
@dynamic model;
@dynamic nextCaseNumber;
@dynamic status;
@dynamic photo;

@end
