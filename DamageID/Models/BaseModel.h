//
//  BaseModel.h
//  DamageID
//
//  Created by Davor Galambos on 14/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

-(NSString *)parseString:(id)data;
-(NSNumber *)parseStringToNumberLong:(NSString *)inputStr;

@end
