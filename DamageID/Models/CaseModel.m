//
//  CaseModel.m
//  DamageID
//
//  Created by Davor Galambos on 20/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "CaseModel.h"

@implementation CaseModel


/*
 Printing description of dictionary:
 {
 carId = 561;
 caseNumber = 51969;
 licensePlate = 2222;
 model = 2222;
 msg = 4;
 photosAfter = 0;
 photosBefore = 0;
 status = 0;
 thumbnailUrl = "https://damagecar.s3.amazonaws.com/1/561.jpg?1436889355415";
 unitNumber = 2222;
 }
 
 
 {
 carId = 517;
 caseNumber = 52077;
 licensePlate = adfafkjl;
 model = "BMW i8";
 msg = 4;
 photosAfter = 0;
 photosBefore = 0;
 status = 0;
 thumbnailUrl = "https://damagecar.s3.amazonaws.com/1/517.jpg?1436977702139";
 unitNumber = 9999;
 }
 */
- (CaseModel *)initWithDictionary:(NSDictionary*)dictionary {
    if (self = [super init]) {
        //NSScanner *scanner = [NSScanner scannerWithString:testString];
        //BOOL isNumeric =
        //self.status = nil;
        //NSString *s=[dictionary[@"carId"] class];
        _msg = [[self parseStringToNumberLong:dictionary[@"msg"]] longValue];
        //self.msg = [dictionary[@"msg"] longValue];
        _caseNumber = [[self parseStringToNumberLong:dictionary[@"caseNumber"]] longValue];
        //self.caseNumber = [dictionary[@"caseNumber"] longValue];
        _status = [[self parseStringToNumberLong:dictionary[@"status"]] longValue];
        //self.status = [dictionary[@"status"] longValue];
        _carId = [self parseStringToNumberLong:dictionary[@"carId"]];
        //self.carId = [self parseStringToNumberLong:dictionary[@"carId"]];//[nf numberFromString:dictionary[@"carId"]] != nil ? [nf numberFromString: dictionary[@"carId"]] : 0;
        
        //NSLog(@"%ld", [self.carId longValue]);
        //self.unitNumber = dictionary[@"unitNumber"];
        _unitNumber = [self parseString:dictionary[@"unitNumber"]];
        _licensePlate = [self parseString:dictionary[@"licensePlate"]];
        _model = [self parseString:dictionary[@"model"]];
        _thumbnailUrl = [self parseString:dictionary[@"thumbnailUrl"]];
        _photosBefore = [[self parseStringToNumberLong:dictionary[@"photosBefore"]] longValue];
        _photosAfter = [[self parseStringToNumberLong:dictionary[@"photosAfter"]] longValue];

        //self.photosBefore = [dictionary[@"photosBefore"] longValue];
        //self.photosAfter = [dictionary[@"photosAfter"] longValue];
 
    }
    return self;
}

@end
