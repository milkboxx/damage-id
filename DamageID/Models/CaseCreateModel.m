//
//  CaseCreateModel.m
//  DamageID
//
//  Created by Davor Galambos on 14/07/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import "CaseCreateModel.h"

@implementation CaseCreateModel

- (CaseCreateModel *)initWithDictionary:(NSDictionary*)dictionary {
    if (self = [super init]) {
        _msg = [[self parseStringToNumberLong:dictionary[@"msg"]] longValue];
        _caseNumber = [[self parseStringToNumberLong:dictionary[@"caseNumber"]] longValue];
        _licensePlate = [self parseString:dictionary[@"licensePlate"]];
        _insurance = [[self parseStringToNumberLong:dictionary[@"insurance"]] longValue];
    }
    return self;
}

@end
