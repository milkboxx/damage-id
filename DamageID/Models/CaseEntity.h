//
//  CaseEntity.h
//  
//
//  Created by Davor Galambos on 23/07/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PhotoEntity;

@interface CaseEntity : NSManagedObject

@property (nonatomic, retain) NSString * accessKey;
@property (nonatomic, retain) NSNumber * beforeAfter;
@property (nonatomic, retain) NSNumber * caseNumber;
@property (nonatomic, retain) NSNumber * insurance;
@property (nonatomic, retain) NSString * licencePlate;
@property (nonatomic, retain) NSString * model;
@property (nonatomic, retain) NSNumber * nextCaseNumber;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSSet *photo;
@end

@interface CaseEntity (CoreDataGeneratedAccessors)

- (void)addPhotoObject:(PhotoEntity *)value;
- (void)removePhotoObject:(PhotoEntity *)value;
- (void)addPhoto:(NSSet *)values;
- (void)removePhoto:(NSSet *)values;

@end
