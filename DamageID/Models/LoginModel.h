//
//  LoginModel.h
//  DamageID
//
//  Created by Davor Galambos on 19/06/15.
//  Copyright (c) 2015 DamageID. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface LoginModel : BaseModel

@property (nonatomic, strong) NSString *accessKey;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, assign) long photoVersion;
@property (nonatomic, strong) NSString *username;

- (LoginModel *)initWithDictionary:(NSDictionary*)dictionary;
    
@end
